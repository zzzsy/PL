(* 只有y>=0才可以 *)
fun pow(x:int, y:int) = 
    if y = 0 
    then 1
    else
    x*pow(x,y-1)

fun cube(x: int) = pow(x,3)

val n = cube(7)

val tp = (2,3)

val tp = pow tp
