val a = []

val c = [1,2,3]

val one = hd c
(* return the first ele of list *)

val two_three = tl c

val noo = null c

(* return if c is NULL *)

fun reverse nil = nil | reverse (x::xs) = (reverse xs) @ [x];

val ret = reverse(c);
