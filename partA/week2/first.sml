(* 这是一个注释 *)

(* Variable Bindings *)

val x = 34; (* int *)
val y = 17;
(* static environment: x: int, y: int *)
(* dynamic environment: x-->34  y-->17 *)

val z = (x + y) + 1;
(* Expressions look up val in dynamic environment && static environment *)
(* static environment: x: int, y: int, z: int *)
(* dynamic environment: x-->34  y-->17 z-->52*)

val abs_of _z = if z < 0 then 0-z else z; (* bool int *)
(* A condition expression  *)
(* As for type checking，"then" and "else" have the same type->int *)

val abs_of_z_simpler = abs z;
(* function *)
