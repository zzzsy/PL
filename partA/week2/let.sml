fun countup_from1_better (x : int) =
    let fun count (from : int) =
        if from = x
        then x :: []
        else from :: count (from + 1)
    in
        count 3
    end

val ret = countup_from1_better(5);
